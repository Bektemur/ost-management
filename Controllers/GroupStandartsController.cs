﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using WebApplication;

namespace WebApplication.Controllers
{
    public class GroupStandartsController : Controller
    {
        private readonly DiplomnayaContext _context;

        public GroupStandartsController(DiplomnayaContext context)
        {
            _context = context;
        }

        // GET: GroupStandarts
        public async Task<IActionResult> Index()
        {
            return View(await _context.GroupStandarts.ToListAsync());
        }

        // GET: GroupStandarts/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var groupStandarts = await _context.GroupStandarts
                .FirstOrDefaultAsync(m => m.IdStandards == id);
            if (groupStandarts == null)
            {
                return NotFound();
            }

            return View(groupStandarts);
        }

        // GET: GroupStandarts/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: GroupStandarts/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("IdStandards,StandardsName")] GroupStandarts groupStandarts)
        {
            if (ModelState.IsValid)
            {
                _context.Add(groupStandarts);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(groupStandarts);
        }

        // GET: GroupStandarts/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var groupStandarts = await _context.GroupStandarts.FindAsync(id);
            if (groupStandarts == null)
            {
                return NotFound();
            }
            return View(groupStandarts);
        }

        // POST: GroupStandarts/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("IdStandards,StandardsName")] GroupStandarts groupStandarts)
        {
            if (id != groupStandarts.IdStandards)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(groupStandarts);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!GroupStandartsExists(groupStandarts.IdStandards))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(groupStandarts);
        }

        // GET: GroupStandarts/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var groupStandarts = await _context.GroupStandarts
                .FirstOrDefaultAsync(m => m.IdStandards == id);
            if (groupStandarts == null)
            {
                return NotFound();
            }

            return View(groupStandarts);
        }

        // POST: GroupStandarts/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var groupStandarts = await _context.GroupStandarts.FindAsync(id);
            _context.GroupStandarts.Remove(groupStandarts);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool GroupStandartsExists(int id)
        {
            return _context.GroupStandarts.Any(e => e.IdStandards == id);
        }
    }
}
