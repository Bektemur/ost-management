﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using WebApplication;

namespace WebApplication.Controllers
{
    public class NormativeWorksController : Controller
    {
        private readonly DiplomnayaContext _context;

        public NormativeWorksController(DiplomnayaContext context)
        {
            _context = context;
        }

        // GET: NormativeWorks
        public async Task<IActionResult> Index()
        {
            var diplomnayaContext = _context.NormativeWork.Include(n => n.Standarts).Include(n => n.Unit);
            return View(await diplomnayaContext.ToListAsync());
        }

        // GET: NormativeWorks/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var normativeWork = await _context.NormativeWork
                .Include(n => n.Standarts)
                .Include(n => n.Unit)
                .FirstOrDefaultAsync(m => m.IdNormativeWork == id);
            if (normativeWork == null)
            {
                return NotFound();
            }

            return View(normativeWork);
        }

        // GET: NormativeWorks/Create
        public IActionResult Create()
        {
            ViewData["StandartsId"] = new SelectList(_context.GroupStandarts, "IdStandards", "IdStandards");
            ViewData["UnitId"] = new SelectList(_context.Unit, "IdUnit", "UnitName");
            return View();
        }

        // POST: NormativeWorks/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("IdNormativeWork,StandartsId,NormativeWorkName,UnitId,NormativeValue")] NormativeWork normativeWork)
        {
            if (ModelState.IsValid)
            {
                _context.Add(normativeWork);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["StandartsId"] = new SelectList(_context.GroupStandarts, "IdStandards", "StandardsName", normativeWork.StandartsId);
            ViewData["UnitId"] = new SelectList(_context.Unit, "IdUnit", "UnitName", normativeWork.UnitId);
            return View(normativeWork);
        }

        // GET: NormativeWorks/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var normativeWork = await _context.NormativeWork.FindAsync(id);
            if (normativeWork == null)
            {
                return NotFound();
            }
            ViewData["StandartsId"] = new SelectList(_context.GroupStandarts, "IdStandards", "IdStandards", normativeWork.StandartsId);
            ViewData["UnitId"] = new SelectList(_context.Unit, "IdUnit", "UnitName", normativeWork.UnitId);
            return View(normativeWork);
        }

        // POST: NormativeWorks/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("IdNormativeWork,StandartsId,NormativeWorkName,UnitId,NormativeValue")] NormativeWork normativeWork)
        {
            if (id != normativeWork.IdNormativeWork)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(normativeWork);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!NormativeWorkExists(normativeWork.IdNormativeWork))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["StandartsId"] = new SelectList(_context.GroupStandarts, "IdStandards", "IdStandards", normativeWork.StandartsId);
            ViewData["UnitId"] = new SelectList(_context.Unit, "IdUnit", "UnitName", normativeWork.UnitId);
            return View(normativeWork);
        }

        // GET: NormativeWorks/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var normativeWork = await _context.NormativeWork
                .Include(n => n.Standarts)
                .Include(n => n.Unit)
                .FirstOrDefaultAsync(m => m.IdNormativeWork == id);
            if (normativeWork == null)
            {
                return NotFound();
            }

            return View(normativeWork);
        }

        // POST: NormativeWorks/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var normativeWork = await _context.NormativeWork.FindAsync(id);
            _context.NormativeWork.Remove(normativeWork);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool NormativeWorkExists(int id)
        {
            return _context.NormativeWork.Any(e => e.IdNormativeWork == id);
        }
    }
}
