﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace WebApplication
{
    public partial class DiplomnayaContext : DbContext
    {
        public DiplomnayaContext()
        {
        }

        public DiplomnayaContext(DbContextOptions<DiplomnayaContext> options)
            : base(options)
        {
        }

        public virtual DbSet<GroupStandarts> GroupStandarts { get; set; }
        public virtual DbSet<NormativeWork> NormativeWork { get; set; }
        public virtual DbSet<Unit> Unit { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer("Server=DESKTOP-7639HT7\\MSSQLSERVERS;Database=Diplomnaya;Trusted_Connection=True;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<GroupStandarts>(entity =>
            {
                entity.HasKey(e => e.IdStandards);

                entity.Property(e => e.IdStandards).HasColumnName("ID_Standards");

                entity.Property(e => e.StandardsName)
                    .HasColumnName("Standards_Name")
                    .HasMaxLength(200);
            });

            modelBuilder.Entity<NormativeWork>(entity =>
            {
                entity.HasKey(e => e.IdNormativeWork);

                entity.Property(e => e.IdNormativeWork).HasColumnName("ID_NormativeWork");

                entity.Property(e => e.NormativeValue).HasColumnType("decimal(18, 0)");

                entity.Property(e => e.NormativeWorkName).HasMaxLength(200);

                entity.Property(e => e.StandartsId).HasColumnName("Standarts_ID");

                entity.Property(e => e.UnitId).HasColumnName("Unit_ID");

                entity.HasOne(d => d.Standarts)
                    .WithMany(p => p.NormativeWork)
                    .HasForeignKey(d => d.StandartsId)
                    .HasConstraintName("FK_NormativeWork_GroupStandarts");

                entity.HasOne(d => d.Unit)
                    .WithMany(p => p.NormativeWork)
                    .HasForeignKey(d => d.UnitId)
                    .HasConstraintName("FK_NormativeWork_Unit");
            });

            modelBuilder.Entity<Unit>(entity =>
            {
                entity.HasKey(e => e.IdUnit);

                entity.Property(e => e.IdUnit).HasColumnName("ID_unit");

                entity.Property(e => e.UnitName)
                    .IsRequired()
                    .HasColumnName("Unit_Name")
                    .HasMaxLength(200);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
