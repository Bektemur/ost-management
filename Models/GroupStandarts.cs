﻿using System;
using System.Collections.Generic;

namespace WebApplication
{
    public partial class GroupStandarts
    {
        public GroupStandarts()
        {
            NormativeWork = new HashSet<NormativeWork>();
        }

        public int IdStandards { get; set; }
        public string StandardsName { get; set; }

        public virtual ICollection<NormativeWork> NormativeWork { get; set; }
    }
}
