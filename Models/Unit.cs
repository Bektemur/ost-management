﻿using System;
using System.Collections.Generic;

namespace WebApplication
{
    public partial class Unit
    {
        public Unit()
        {
            NormativeWork = new HashSet<NormativeWork>();
        }

        public int IdUnit { get; set; }
        public string UnitName { get; set; }

        public virtual ICollection<NormativeWork> NormativeWork { get; set; }
    }
}
