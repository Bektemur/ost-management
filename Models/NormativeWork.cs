﻿using System;
using System.Collections.Generic;

namespace WebApplication
{
    public partial class NormativeWork
    {
        public int IdNormativeWork { get; set; }
        public int? StandartsId { get; set; }
        public string NormativeWorkName { get; set; }
        public int? UnitId { get; set; }
        public decimal? NormativeValue { get; set; }

        public virtual GroupStandarts Standarts { get; set; }
        public virtual Unit Unit { get; set; }
    }
}
